const routes = [

  {
    path: '/',
    redirect: 'docs'
  },
  {
    path: '/docs',
    component: () => import('layouts/MainLayout.vue'),
    children: [{
      path: '',
      component: () => import('components/document-page.vue')
    }, ]
  }, {
    path: '/simple',
    component: () => import('layouts/MainLayout.vue'),
    children: [{
      path: '',
      component: () => import('components/simple-component.vue')
    }, ]
  },
  {
    path: '/columns',
    component: () => import('layouts/MainLayout.vue'),
    children: [{
      path: '',
      component: () => import('components/bootstrap/12-columns.vue')
    }, ]
  },
  {
    path: '/responsive-columns',
    component: () => import('layouts/MainLayout.vue'),
    children: [{
      path: '',
      component: () => import('components/bootstrap/responsive-columns.vue')
    }, ]
  },
  // {
  //   path: '/documentList',
  //   component: () => import('layouts/MainLayout.vue'),
  //   children: [{
  //     path: '',
  //     component: () => import('components/DocumentList.vue')
  //   }]
  // },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
